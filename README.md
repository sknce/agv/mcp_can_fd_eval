# Mcp CAN FD Eval
KiCad 7 project for CAN FD eval board. Can be placed on breadboard.

## Board contents:
* CAN FD SPI Controller
* CAN FD Transciever
* 40 MHz quartz crystal
* Capacitors
* Terminating resistors and jumpers to engage or disengage them

## Author:
Maksymilian Grabowy